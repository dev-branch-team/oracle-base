# Drupal8 on PHP7 with Oracle 19c Traefik2 based installation.

## Short version.
```
git clone git@bitbucket.org:dev-branch-team/oracle-base.git oracle &&\
cd oracle   && docker-compose up -d &&\
cd codebase && composer install
```

## Detailed version.
 
1] Clone this repo: 
```
git clone git@bitbucket.org:dev-branch-team/oracle-base.git oracle
```

2] Pull all needed containers (nginx, PHP7 with pdo_oci extension, Oracle):
```
cd oracle
docker-compose up -d
```
This can take up to 10 minutes to build everything 
(check logs of oracle_database container for the status).

3] Install Drupal 8 with all needed requirements.
```
cd codebase
composer install
```

4] Configure your Traefik by adding `oracle_default` network.
Then, go to http://oracle.docker.localhost in your browser.

5] Install Drupal. Connection details are:
```
   'database' => 'ORCLPDB1',
   'username' => 'SYSTEM',
   'password' => 'ORCLpwd1',
   'host' => 'oracle',
   'port' => '1521',
   'prefix' => '',
```
