oracle/database:19.3.0-ee build manual

1. Update the build script from:
https://github.com/oracle/docker-images/tree/master/OracleDatabase/SingleInstance/dockerfiles/19.3.0

2. Place the Oracle Database source into appropriate version folder from:
https://www.oracle.com/database/technologies/oracle-database-software-downloads.html

3. Check the structure. It should be like:
<repo_root>/build/oracle/19.3.0 = script from clause 1
<repo_root>/build/oracle/19.3.0/LINUX.X64_193000_db_home.zip = archive from clause 2

4. Build local Oracle image like:
<repo_root>/build/oracle/buildDockerImage.sh -v 19.3.0 -e

5. Update your docker-compose to use the newly built local image. Like:
image: oracle/database:19.3.0-ee

6. Set the password:
docker-compose exec oracle ./setPassword.sh ORCLpwd1
