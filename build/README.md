# Repo push notes.

## oracle_database

`docker tag oracle/database:19.3.0-ee devbranch/oracle_database:19.3.0-ee`

`docker push devbranch/oracle_database:19.3.0-ee`

## phpoci

`docker tag oracle_phpoci devbranch/phpoci:7.1-fpm`

`docker push devbranch/phpoci:7.1-fpm`
